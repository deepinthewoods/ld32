package com.mygdx.game.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.mygdx.game.Main;
import com.badlogic.gdx.tools.texturepacker.TexturePacker;

public class DesktopLauncher {
	public static void main (String[] arg) {
		
		/*String inputDir = "c:/sprites/blocks/hasNormal";
		String outputDir = "C:/_LD32/android/assets";
		String packFileName = "diffuse";
		TexturePacker.process(inputDir, outputDir, packFileName);
		
		inputDir = "c:/sprites/blocks/normal";
		outputDir = "C:/_LD32/android/assets";
		packFileName = "normal";
		TexturePacker.process(inputDir, outputDir, packFileName);*/
		
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = 800;
		config.height = 600;
		new LwjglApplication(new Main(), config);
	}
}
