package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox.CheckBoxStyle;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton.ImageButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.ImageTextButton;
import com.badlogic.gdx.scenes.scene2d.ui.ImageTextButton.ImageTextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.badlogic.gdx.scenes.scene2d.utils.BaseDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;

public class SkinUtil {
	private static final int NUMBER_OF_ITEMS = 6;
	private static NinePatchDrawable[] tableBack;
	public static SpriteDrawable[] itemSprites;
	public static ButtonStyle[] buttonStyles;
	public static BitmapFont makeSkin(Skin skin, TextureAtlas atlas, TextureAtlas itemAtlas) {
        //BitmapFont font = new BitmapFont(Gdx.files.internal("andale.fnt"), atlas.findRegion("fonts"));//, Gdx.files.internal("data/font/fonts.png"));
		BitmapFont font = new BitmapFont(Gdx.files.internal("andale.fnt"), Gdx.files.internal("andale.png"), false);
        //Gdx.app.log(TAG,  "bleh"+Gdx.files.internal("data/ini/Play/assets.ini").exists());;
       // font.setScale(1f);
		
        
        TextureRegion reg = atlas.findRegion("button");
        NinePatchDrawable patch = new NinePatchDrawable(new NinePatch(reg));
        patch.getPatch().setColor(Color.DARK_GRAY);
        float border = 0;//Gdx.graphics.getHeight()/50f;
        
        

        TextureRegion blockSel = atlas.findRegion("buttonselected");
        NinePatch blockSel9 = new NinePatch(blockSel);
        blockSel9.setMiddleWidth(2);
        blockSel9.setMiddleHeight(2);

        
        NinePatchDrawable sliderBack = new NinePatchDrawable(new NinePatch(atlas.findRegion("slider")));
        NinePatchDrawable knob = new NinePatchDrawable(new NinePatch(reg));
        NinePatchDrawable cursor = new NinePatchDrawable(new NinePatch(reg));
        NinePatchDrawable selection = new NinePatchDrawable(new NinePatch(reg));
        NinePatchDrawable scroll = new NinePatchDrawable(new NinePatch(reg));
        NinePatchDrawable sliderKnob = new NinePatchDrawable(new NinePatch(atlas.findRegion("sliderknob")));
        NinePatchDrawable checked = new NinePatchDrawable(new NinePatch(reg));

        NinePatchDrawable back = new NinePatchDrawable(new NinePatch(reg));
        NinePatchDrawable up = new NinePatchDrawable(new NinePatch(reg));
        NinePatchDrawable down = new NinePatchDrawable(new NinePatch(reg));
        NinePatchDrawable upB = new NinePatchDrawable(new NinePatch(reg));
        NinePatchDrawable downB = new NinePatchDrawable(new NinePatch(reg));
        NinePatchDrawable midB = new NinePatchDrawable(new NinePatch(reg));
        NinePatchDrawable tfBack = new NinePatchDrawable(new NinePatch(reg));
        
        itemSprites = new SpriteDrawable[NUMBER_OF_ITEMS];
        buttonStyles = new ButtonStyle[NUMBER_OF_ITEMS];
        for (int i = 0; i < NUMBER_OF_ITEMS; i++){
        	Sprite s = itemAtlas.createSprite("item", i);
        	itemSprites[i] = new SpriteDrawable(s);
        	buttonStyles[i] = new ImageButtonStyle(up, down, upB, itemSprites[i], itemSprites[i], itemSprites[i]);
        }
        
        tableBack = new NinePatchDrawable[50];
        for (int i = 0; i < tableBack.length; i++){
        	tableBack[i] = new NinePatchDrawable(new NinePatch(reg));
        }
        
        Color color = new Color(Color.WHITE);
        TextButton.TextButtonStyle tbStyle = new TextButton.TextButtonStyle(up, down, up, font);
        TextButton.TextButtonStyle tbStyleBoolean = new TextButton.TextButtonStyle(upB, midB, downB, font);

        //TextButton.TextButtonStyle tbStyleToggle = new TextButton.TextButtonStyle(up, down, checked, font);
        
        tbStyleBoolean.checkedFontColor = Color.WHITE;
        tbStyleBoolean.fontColor = Color.LIGHT_GRAY;
        
        tbStyle.checkedFontColor = Color.WHITE;
        tbStyle.fontColor = Color.LIGHT_GRAY;
        
        Button.ButtonStyle butStyle = new Button.ButtonStyle(up, down, back);
        Button.ButtonStyle butStyleToggle = new Button.ButtonStyle(up, down, checked);
        
        Touchpad.TouchpadStyle tpStyle = new Touchpad.TouchpadStyle(back, knob);

        TextField.TextFieldStyle tfStyle = new TextField.TextFieldStyle(font, color, cursor, selection, tfBack);
        
        Label.LabelStyle lStyle = new Label.LabelStyle(font, color);
        lStyle.background = back;
        
        //float h = Gdx.graphics.getHeight()/8f, w = Gdx.graphics.getWidth()/2f;
        float h = 5, w = 5;
        lStyle.background.setMinHeight(h);
        sliderBack.setMinHeight(h);
        sliderBack.setMinWidth(w);
        setBorder(sliderBack, border);
        //sliderBack.setLeftWidth(w);

        back.setMinHeight(h);
        back.setMinWidth(w);
        setBorder(back, border);
        
        Color textFieldColor = new Color(.477f, .477f, .477f, 1f);
        tfBack.setMinHeight(h);
        tfBack.setMinWidth(w);
        setBorder(tfBack, border);
        tfBack.getPatch().setColor(textFieldColor);
        
        Color tableColor = new Color(.8477f, .8477f, .8477f, 1f);
        HSVColor col = new HSVColor(0);
        col.h = 0f;
        col.s = .3713f;
        col.v = .692f;
        for (int i = 0; i < tableBack.length; i++){
        	col.h += 1f/tableBack.length;
        	col.toRGB(tableColor);
        	//tableColor.a = .645f;
        	tableBack[i].setMinHeight(h);
        	tableBack[i].setMinWidth(h);
        	setBorder(tableBack[i], 5);
        	tableBack[i].getPatch().setColor(tableColor);
        }
        
        
        
        
        Color tbCol = new Color(0f, 0f, 0f, .315f);
        up.setMinHeight(h);
        up.setMinWidth(h);
        setBorder(up, border);
        up.getPatch().setColor(tbCol);
        
        Color tfbooleanCol = new Color(Color.DARK_GRAY);
        upB.setMinHeight(h);
        upB.setMinWidth(h);
        setBorder(upB, border);
        upB.getPatch().setColor(tfbooleanCol);
        
        Color tfbooleanColDown = new Color(Color.DARK_GRAY);
        downB.setMinHeight(h);
        downB.setMinWidth(h);
        setBorder(downB, border);
        downB.getPatch().setColor(tfbooleanColDown);
        
        Color tfbooleanColDownm = new Color(Color.DARK_GRAY);
        midB.setMinHeight(h);
        midB.setMinWidth(h);
        setBorder(midB, border);
        midB.getPatch().setColor(tfbooleanColDownm);
        
        down.setMinHeight(h);
        down.setMinWidth(h);
        setBorder(down, border);
        down.getPatch().setColor(Color.LIGHT_GRAY);
        //back2.setMinHeight(h);
        //back2.setMinWidth(h);
        //setBorder(back2, border);
        
        
        //back3.setMinHeight(h);
        //back3.setMinWidth(h);
        //setBorder(back3, border);
        
        checked.setMinHeight(h);
        checked.setMinWidth(h);
        setBorder(checked, border);
        checked.getPatch().setColor(Color.GREEN);
        
        patch.setMinHeight(h);
        patch.setMinWidth(w);
        setBorder(patch, border);
        back.setMinHeight(h);
        back.setMinWidth(w);
        setBorder(back, border);
        back.getPatch().setColor(Color.DARK_GRAY);
        //back.setLeftWidth(5);
       // back.setRightWidth(5);
        
        sliderKnob.setMinHeight(h+8);
        //sliderKnob.setMinWidth(w+8);
        sliderBack.setLeftWidth(4);sliderBack.setRightWidth(4);
        Slider.SliderStyle sliderStyle = new Slider.SliderStyle(sliderBack, sliderKnob);
        
        //SlideColorPicker.SlideColorPickerStyle colorSliderStyle = new SlideColorPicker.SlideColorPickerStyle();
       // colorSliderStyle.knob = sliderKnob;
        //colorSliderStyle.background = sliderBack;
        Label.LabelStyle labeStyle = new Label.LabelStyle(font, color);
        
        ScrollPane.ScrollPaneStyle scrollStyle = new ScrollPane.ScrollPaneStyle(back, scroll, knob, scroll, knob);

        //TextFieldStyle textFieldStyle = new TextField.TextFieldStyle(fontm Color.WHITE, cursor, selection, back);
        labeStyle.background = back;
        
        CheckBox.CheckBoxStyle cbStyle = new CheckBoxStyle(down, up, font, color);
        
        //skin.add("default", textFieldStyle);
       // skin.add("default", tbStyle);
        //skin.add("toggle", tbStyleToggle);

        ImageButton.ImageButtonStyle iStyle = new ImageButtonStyle(up, down, checked, up, up, up);
        ImageTextButton.ImageTextButtonStyle itStyle = new ImageTextButtonStyle(tbStyle);
        
        
        skin.add("default", iStyle);
        skin.add("default", itStyle);

        skin.add("default", butStyle);
        skin.add("default", tbStyle);
        skin.add("boolean", tbStyleBoolean);
        //skin.add("toggle", tbStyleToggle2);
        skin.add("default", tfStyle);
        //skin.add("toggle", tbStyleToggle);
        //skin.add("block", butStyleBlock);
        
        //skin.add("default", lStyle);
        skin.add("default", cbStyle);

        //skin.add("default", tpStyle);
        //skin.add("default", tfStyle);
        
        //skin.add("default-horizontal", sliderStyle);
        //skin.add("default-horizontal", colorSliderStyle);
        skin.add("default", labeStyle);
        //skin.add("default", scrollStyle);
        //skin.add("default", tableStyle);
        
        return font;

    }
	 private static void setBorder(BaseDrawable draw, float border){
	        draw.setBottomHeight(border); draw.setTopHeight(border);
	        draw.setLeftWidth(border); draw.setRightWidth(border);
	    }
}
