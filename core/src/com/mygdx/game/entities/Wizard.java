package com.mygdx.game.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.ui.ButtonGroup;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pools;
import com.mygdx.game.Animations;
import com.mygdx.game.Entity;
import com.mygdx.game.EntityGroup;
import com.mygdx.game.Main;
import com.mygdx.game.Map;

public class Wizard extends Entity {

	
	
	private boolean throwQueued;
	
	@Override
	public void onSpawn() {
		animNameID = Animations.WIZARD_ID;
		
	}
	public Wizard(){
		
	}

	public void doThrow(EntityGroup allies) {
		//Gdx.app.log("player", "throw"+inv[getActiveInv()]);;
		//amounts[getActiveInv()]--;
		
		int item = 2;//inv[getActiveInv()];
		Entity bullet = null;
		switch (item){
		case 0:bullet = Pools.obtain(SwordBullet.class);break;
		case 1:bullet = Pools.obtain(AxeBullet.class);break;
		case 2:bullet = Pools.obtain(DaggerBullet.class);break;
		case 3:bullet = Pools.obtain(YellowPotionBullet.class);break;
		case 4:bullet = Pools.obtain(BluePotionBullet.class);break;
		case 5:bullet = Pools.obtain(RedPotionBullet.class);break;
		}
		bullet.direction = direction;
		bullet.mapPosition.set(mapPosition);
		bullet.onSpawn();
		allies.add(bullet);
		Main.queueInvUpdate = true;
		//inv[getActiveInv()] = 0;
		
	}

	


	public boolean isThrowing() {
		if (throwQueued){
			throwQueued = false;
			return true;
		}
		return false;
	}

	
	



	@Override
	public boolean strategize(Map map, EntityGroup enemies, EntityGroup allies) {
		moves--;
		if (animID == Animations.DIE) return false;;
		Array<Entity> playerArr = enemies.closestEntity(this);
		int dir = MathUtils.random(3);
		if (playerArr.size > 0){
			Player player = (Player) playerArr.get(0);
			
			int dx = mapPosition.x - player.mapPosition.x;
			int dy = mapPosition.y - player.mapPosition.y;
			int ex = Math.abs(dx);
			int ey = Math.abs(dy);
			
			if (player.mapPosition.x == mapPosition.x){
				if (dy > 0){
					dir = Main.UP;
					//while (dir == Main.UP)dir = MathUtils.random(3); 
					//if (dir != Main.DOWN) do{dir = MathUtils.random(3);}while (dir == Main.UP);
					
					//if (dir != Main.DOWN) do{dir = MathUtils.random(3);}while (dir == Main.UP);
				} else {
					//while (dir == Main.DOWN)dir = MathUtils.random(3); 
					//if (dir != Main.UP) do{dir = MathUtils.random(3);}while (dir == Main.DOWN);
					dir = Main.DOWN;
					//if (dir != Main.UP) do{dir = MathUtils.random(3);}while (dir == Main.DOWN);

				}
			} else if (player.mapPosition.y == mapPosition.y){
				if (dx < 0){
					//while (dir == Main.LEFT)dir = MathUtils.random(3); 
					//if (dir != Main.RIGHT) do{dir = MathUtils.random(3);}while (dir == Main.LEFT);
					dir = Main.RIGHT;
					//if (dir != Main.RIGHT) do{dir = MathUtils.random(3);}while (dir == Main.LEFT);

				} else {
					//while (dir == Main.RIGHT)dir = MathUtils.random(3); 
					//if (dir != Main.LEFT) do{dir = MathUtils.random(3);}while (dir == Main.RIGHT);
					dir = Main.LEFT;
					//if (dir != Main.LEFT) do{dir = MathUtils.random(3);}while (dir == Main.RIGHT);

				}
				tryToThrow(dir, map, allies);
				return false;
			}
			
			if (ey > ex){
				if (dy > 0){
					while (dir == Main.UP)dir = MathUtils.random(3); 
					if (dir != Main.DOWN) do{dir = MathUtils.random(3);}while (dir == Main.UP);
					
					//if (dir != Main.DOWN) do{dir = MathUtils.random(3);}while (dir == Main.UP);
				} else {
					while (dir == Main.DOWN)dir = MathUtils.random(3); 
					if (dir != Main.UP) do{dir = MathUtils.random(3);}while (dir == Main.DOWN);
					//if (dir != Main.UP) do{dir = MathUtils.random(3);}while (dir == Main.DOWN);

				}
			} else {
				if (dx < 0){
					while (dir == Main.LEFT)dir = MathUtils.random(3); 
					if (dir != Main.RIGHT) do{dir = MathUtils.random(3);}while (dir == Main.LEFT);
					//if (dir != Main.RIGHT) do{dir = MathUtils.random(3);}while (dir == Main.LEFT);

				} else {
					while (dir == Main.RIGHT)dir = MathUtils.random(3); 
					if (dir != Main.LEFT) do{dir = MathUtils.random(3);}while (dir == Main.RIGHT);
					//if (dir != Main.LEFT) do{dir = MathUtils.random(3);}while (dir == Main.RIGHT);

				}
			}
			
		}
		this.tryToMove(dir, map, enemies);
		//Gdx.app.lof("bat", "djklsf;lk");
		
		return false;
	}


	public boolean tryToThrow(int dir, Map map, EntityGroup playerEntities) {
		//if (inv[getActiveInv()] != 0){
		//if (amounts[getActiveInv()] <= 0) return false;
		
		
		
		this.direction = dir;
		switch (direction){
		case Main.UP:
			tmpV.set(0,1);
			break;
		case Main.DOWN:
			tmpV.set(0,-1);
			break;
		case Main.LEFT:
			tmpV.set(-1,0);
			break;
		case Main.RIGHT:
			tmpV.set(1,0);
			break;
		}
		tmpV.x += mapPosition.x;
		tmpV.y += mapPosition.y;
		
		if (map.isEmpty(tmpV.x, tmpV.y)){
			
			//direction = dir;
			throwQueued = true;
			moveTime = 0;
			finishedMoving = false;
			animTime = 0f;
			animID = Animations.THROW;
		}
		//return true;
		//moves = 1;
		//Gdx.app.log("pl", "THROW");
		//}
		return true;
		
	}
	public void takeDamage(float f, Map map){
		super.takeDamage(1f, map);
		map.createItem(mapPosition);
	}
	
	@Override
	public void onStartMove() {
		//Gdx.app.log("player", "star move");
		moves = 1;
		
	}
	@Override
	public void onFinishedMoving(Map map, EntityGroup allies, EntityGroup items, EntityGroup enemies) {
		//arr = items.getForPosition(mapPosition.x, mapPosition.y);
		tmpV.set(mapPosition.x+.5f, mapPosition.y+.5f);
		Array<Entity> arr = items.getForPosition(tmpV);
		for (int i = arr.size-1; i >= 0; i--){
			Entity item = arr.get(i);
			items.removeValue(item, true);
			
			//addToInv(item.animNameID);
				
			
			Pools.free(item);
		}
		
	}
	

}
