package com.mygdx.game.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pools;
import com.mygdx.game.Animations;
import com.mygdx.game.Entity;
import com.mygdx.game.EntityGroup;
import com.mygdx.game.Main;
import com.mygdx.game.Map;

public class DaggerBullet extends Entity {

	@Override
	public boolean strategize(Map map, EntityGroup enemies, EntityGroup allies) {
		
		//Gdx.app.log("bullet", "straSTASTSTSTSTTASTSt"+moves);
		moveTime = 0f;
		finishedMoving = false;
		//animTime = 0f;
		setMovePosition(1);
		moves--;
		maxMoveTime = .05f;
		animID = Animations.SPIN;
		return true;
	}

	private void setMovePosition(float scale) {
		switch (direction){
		case Main.UP:
			tmpV.set(0,1);
			break;
		case Main.DOWN:
			tmpV.set(0,-1);
			break;
		case Main.LEFT:
			tmpV.set(-1,0);
			break;
		case Main.RIGHT:
			tmpV.set(1,0);
			break;
		}
		tmpV.scl(scale);
		tmpV.add(mapPosition.x, mapPosition.y);
		moveToPosition.set(tmpV.x, tmpV.y, 0);
		
	}

	@Override
	public void onSpawn() {
		setMovePosition(1f);
		
		
		onStartMove();
		moveTime = 0;
		animTime = 0f;
		finishedMoving = false;
		moves = 6;
		this.maxMoveTime = .15f;
		this.animNameID = 2;
		animID = Animations.SPIN;
		needsToFinishAnim = false;
		leaveAnimDelta = true;
	}
	
	public void move(float delta, EntityGroup allies, Map map, EntityGroup items, EntityGroup enemies) {
		super.move(delta, allies, map, items, enemies);
		//Gdx.app.log("bullet", "move"+moveTime);
		
	}

	@Override
	public void onStartMove() {
		moves =4;
		
	}

	@Override
	public void onFinishedMoving(Map map, EntityGroup allies, EntityGroup items, EntityGroup enemies) {
		//Gdx.app.log("pl", "finish moving");
		setMovePosition(1);
		
		Array<Entity> arr = enemies.getForPosition(position);
		if (arr.size > 0){
			for (int i = 0; i < arr.size; i++){
				arr.get(i).takeDamage(1f, map);
			}
			
		}
		
		if (!map.isEmpty(moveToPosition.x,  moveToPosition.y)){
			Pools.free(allies.removeValue(this, true));
			Item item = Pools.obtain(Item.class);
			item.itemID = 2;
			item.mapPosition.set(mapPosition);
			item.moveToPosition.set(mapPosition);
			item.position.set(mapPosition.x, mapPosition.y);
			item.onSpawn();
			items.add(item);
		};
	}

}
