package com.mygdx.game.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.ui.ButtonGroup;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pools;
import com.mygdx.game.Animations;
import com.mygdx.game.Entity;
import com.mygdx.game.EntityGroup;
import com.mygdx.game.Main;
import com.mygdx.game.Map;

public class Player extends Entity {

	
	public int[] inv = {0,1,2,3,4,5};
	public int[] amounts = {5, 5, 5, 25, 25, 25};
	private boolean throwQueued;
	private ButtonGroup group;
	@Override
	public void onSpawn() {
		// TODO Auto-generated method stub
		
	}
	public Player(ButtonGroup group){
		this.group = group;
		isPlayer = true;
	}

	public void doThrow(EntityGroup allies) {
		Gdx.app.log("player", "throw"+inv[getActiveInv()]);;
		amounts[getActiveInv()]--;
		
		int item = inv[getActiveInv()];
		Entity bullet = null;
		switch (item){
		case 0:bullet = Pools.obtain(SwordBullet.class);break;
		case 1:bullet = Pools.obtain(AxeBullet.class);break;
		case 2:bullet = Pools.obtain(DaggerBullet.class);break;
		case 3:bullet = Pools.obtain(YellowPotionBullet.class);break;
		case 4:bullet = Pools.obtain(BluePotionBullet.class);break;
		case 5:bullet = Pools.obtain(RedPotionBullet.class);break;
		}
		bullet.direction = direction;
		bullet.mapPosition.set(mapPosition);
		bullet.onSpawn();
		allies.add(bullet);
		Main.queueInvUpdate = true;
		//inv[getActiveInv()] = 0;
		
	}

	public int getActiveInv(){
		int slot = group.getCheckedIndex();
		if (slot < 0) return 0;
		return slot;
	}


	public boolean isThrowing() {
		if (throwQueued){
			throwQueued = false;
			return true;
		}
		return false;
	}

	
	



	@Override
	public boolean strategize(Map map, EntityGroup enemies, EntityGroup allies) {
		moves--;
		//needsToFinishAnim = false;

		return false;
	}


	public boolean tryToThrow(int dir, Map map, EntityGroup playerEntities) {
		//if (inv[getActiveInv()] != 0){
		if (amounts[getActiveInv()] <= 0) return false;
		
		
		
		this.direction = dir;
		switch (direction){
		case Main.UP:
			tmpV.set(0,1);
			break;
		case Main.DOWN:
			tmpV.set(0,-1);
			break;
		case Main.LEFT:
			tmpV.set(-1,0);
			break;
		case Main.RIGHT:
			tmpV.set(1,0);
			break;
		}
		tmpV.x += mapPosition.x;
		tmpV.y += mapPosition.y;
		
		if (map.isEmpty(tmpV.x, tmpV.y)){
			
			//direction = dir;
			throwQueued = true;
			moveTime = 0;
			finishedMoving = false;
			animTime = 0f;
			animID = Animations.THROW;
		}
		//return true;
		//moves = 1;
		//Gdx.app.log("pl", "THROW");
		//}
		return true;
		
	}
	@Override
	public void onStartMove() {
		//Gdx.app.log("player", "star move");
		moves = 1;
		this.extraHealth = true;
		
	}
	
	
	@Override
	public void onFinishedMoving(Map map, EntityGroup allies, EntityGroup items, EntityGroup enemies) {
		//arr = items.getForPosition(mapPosition.x, mapPosition.y);
		tmpV.set(mapPosition.x+.5f, mapPosition.y+.5f);
		Array<Entity> arr = items.getForPosition(tmpV);
		for (int i = arr.size-1; i >= 0; i--){
			Entity item = arr.get(i);
			items.removeValue(item, true);
			
			addToInv(item.animNameID);
				
			
			Pools.free(item);
		}
		if (map.hasHarmfulParticle(mapPosition.x, mapPosition.y)){
			takeDamage(1f, map);
		}
		
	}
	private void addToInv(int animNameID) {
		Main.queueInvUpdate = true;
		for (int i = 0; i < Main.INV_SLOTS; i++){
			if (inv[i] == animNameID){
				amounts[i]++;
			}
		}
		
	}
	public void useSelf() {
		int item = inv[getActiveInv()];
		//Entity bullet = null;
		switch (item){
		case 0://bullet = Pools.obtain(SwordBullet.class);
			
			break;
		case 1://bullet = Pools.obtain(AxeBullet.class);
		break;
		case 2://bullet = Pools.obtain(DaggerBullet.class);
		break;
		case 3://bullet = Pools.obtain(YellowPotionBullet.class);
		break;
		case 4://bullet = Pools.obtain(BluePotionBullet.class);
		break;
		case 5://bullet = Pools.obtain(RedPotionBullet.class);
		break;
		}
		
	}

}
