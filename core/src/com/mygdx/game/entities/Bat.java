package com.mygdx.game.entities;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;
import com.mygdx.game.Animations;
import com.mygdx.game.Entity;
import com.mygdx.game.EntityGroup;
import com.mygdx.game.Main;
import com.mygdx.game.Map;

public class Bat extends Entity {

	@Override
	public boolean strategize(Map map, EntityGroup enemies, EntityGroup allies) {
		if (animID == Animations.DIE) return false;;
		Array<Entity> playerArr = enemies.closestEntity(this);
		int dir = MathUtils.random(3);
		if (playerArr.size > 0){
			Player player = (Player) playerArr.get(0);
			
			int dx = mapPosition.x - player.mapPosition.x;
			int dy = mapPosition.y - player.mapPosition.y;
			int ex = Math.abs(dx);
			int ey = Math.abs(dy);
			if (ey > ex){
				if (dy > 0){
					while (dir == Main.UP)dir = MathUtils.random(3); 
					if (dir != Main.DOWN) do{dir = MathUtils.random(3);}while (dir == Main.UP);
				} else {
					while (dir == Main.DOWN)dir = MathUtils.random(3); 
					if (dir != Main.UP) do{dir = MathUtils.random(3);}while (dir == Main.DOWN);

				}
			} else {
				if (dx < 0){
					while (dir == Main.LEFT)dir = MathUtils.random(3); 
					if (dir != Main.RIGHT) do{dir = MathUtils.random(3);}while (dir == Main.LEFT);

				} else {
					while (dir == Main.RIGHT)dir = MathUtils.random(3); 
					if (dir != Main.LEFT) do{dir = MathUtils.random(3);}while (dir == Main.RIGHT);

				}
			}
			
		}
		this.tryToMove(dir, map, enemies);
		//Gdx.app.lof("bat", "djklsf;lk");
		moves--;
		return false;
	}

	@Override
	public void onSpawn() {
		animNameID = Animations.BAT_ID;
		moveTime = 0;
		animTime = 0f;
		finishedMoving = false;
		this.maxMoveTime = .1f;
		this.needsToFinishAnim = false;
	}

	@Override
	public void onStartMove() {
		moves = 3;
		
	}

	@Override
	public void onFinishedMoving(Map map, EntityGroup allies, EntityGroup items, EntityGroup enemies) {
		// TODO Auto-generated method stub
		
	}

}
