package com.mygdx.game.entities;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.mygdx.game.Animations;
import com.mygdx.game.Entity;
import com.mygdx.game.EntityGroup;
import com.mygdx.game.Map;

public class Item extends Entity {
	public int itemID;
	@Override
	public boolean strategize(Map map, EntityGroup enemies, EntityGroup allies) {
		// TODO Auto-generated method stub
		
		return false;
	}

	float rot = MathUtils.random(1f);
	
	@Override
	public void onSpawn() {
		animNameID = itemID;
		animID = Animations.STAND;
		rot = MathUtils.random(10f);
		//t//his.needsToFinishAnim = false;
		//this.leaveAnimDelta = true;

	}
	
	
	
	@Override
	public void draw(SpriteBatch batch, float delta) {
		animTime = rot;
		animID = Animations.SPIN;
		super.draw(batch, delta);
	}



	@Override
	public void onFinishedMoving(Map map, EntityGroup allies, EntityGroup items, EntityGroup enemies) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStartMove() {
		// TODO Auto-generated method stub

	}

}
