package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class Animations {

	public static final String[] names = {"swordsolo", "axesolo", "daggersolo", "redpotionsolo", "bluepotionsolo", "yellowpotionsolo", "sword", "axe", "dagger", "noweapon", "noweapon", "noweapon", "noweapon", "bat", "wizard"};
	public static Animation[][] animations = new Animation[names.length][64];
	public static final int WALK = 0, STAND = 1, ATTACK = 2, DIE = 3, SPIN = 4, THROW = 5;
	public static final int BAT_ID = 12;
	public static final int SWORD_SOLO_ID = 0;
	public static final int WIZARD_ID = 14;
	
	public static void init(TextureAtlas atlas){
		
		Array<Sprite> keyFrames;// = new Array<Sprite>();
		int offset, length, progress = 0, period = 71;
		
		for (int n = 0; n < names.length; n++){
			progress = 0;
			Gdx.app.log("jkl", "image "+names[n]);
			offset = 1;
			length = 16;
			for (int dir = 0; dir < 4; dir++){
				keyFrames = new Array<Sprite>();
				for (int i = offset; i < offset+length; i++){
					Sprite s = atlas.createSprite(names[n], i+dir*period);
					s.setSize(4f, 4f);
					if (s == null) throw new GdxRuntimeException("sdjkl "+i);
					keyFrames.add(s);
				}
				Animation anim = new Animation(.02f, keyFrames);
				animations[n][progress++] = anim;
			}
			
			offset = 1;
			length = 1;
			for (int dir = 0; dir < 4; dir++){
				keyFrames = new Array<Sprite>();
				for (int i = offset; i < offset+length; i++){
					Sprite s = atlas.createSprite(names[n], i+dir*period);
					s.setSize(4f, 4f);
					if (s == null) throw new GdxRuntimeException("sdjkl "+i);
					keyFrames.add(s);
				}
				Animation anim = new Animation(.03f, keyFrames);
				animations[n][progress++] = anim;
			}
			
			//attack
			offset = 27;
			length = 25;
			for (int dir = 0; dir < 4; dir++){
				keyFrames = new Array<Sprite>();
				for (int i = offset; i < offset+length; i++){
					Sprite s = atlas.createSprite(names[n], i+dir*period);
					s.setSize(4f, 4f);
					if (s == null) throw new GdxRuntimeException("sdjkl "+i);
					keyFrames.add(s);
				}
				Animation anim = new Animation(.03f, keyFrames);
				animations[n][progress++] = anim;
			}
			
			//DIE
			offset = 17;
			length = 2;
			for (int dir = 0; dir < 4; dir++){
				keyFrames = new Array<Sprite>();
				for (int i = offset; i < offset+length; i++){
					Sprite s = atlas.createSprite(names[n], i+dir*period);
					s.setSize(4f, 4f);
					if (s == null) throw new GdxRuntimeException("sdjkl "+i);
					keyFrames.add(s);
				}
				Animation anim = new Animation(.13f, keyFrames);
				animations[n][progress++] = anim;
			}
			
			//SPIN
			offset = 52;
			length = 71-52;
			for (int dir = 0; dir < 4; dir++){
				keyFrames = new Array<Sprite>();
				for (int i = offset; i < offset+length; i++){
					Sprite s = atlas.createSprite(names[n], i+dir*period);
					s.setSize(4f, 4f);
					if (s == null) throw new GdxRuntimeException("sdjkl "+i);
					keyFrames.add(s);
				}
				Animation anim = new Animation(.013f, keyFrames);
				animations[n][progress++] = anim;
			}
			
			offset = 27;
			length = 25;
			for (int dir = 0; dir < 4; dir++){
				keyFrames = new Array<Sprite>();
				for (int i = offset; i < offset+length; i++){
					Sprite s = atlas.createSprite(names[n], i+dir*period);
					s.setSize(4f, 4f);
					if (s == null) throw new GdxRuntimeException("sdjkl "+i);
					keyFrames.add(s);
				}
				Animation anim = new Animation(.012f, keyFrames);
				animations[n][progress++] = anim;
			}
			
			
		}
		
		//BAT
		progress = 0;
		
		offset = 1;
		length = 16;
		for (int dir = 0; dir < 4; dir++){
			keyFrames = new Array<Sprite>();
			for (int i = offset; i < offset+length; i++){
				Sprite s = atlas.createSprite("bat", i+dir*period);
				s.setSize(4f, 4f);
				if (s == null) throw new GdxRuntimeException("sdjkl "+i);
				keyFrames.add(s);
			}
			Animation anim = new Animation(.02f, keyFrames);
			animations[BAT_ID][progress++] = anim;
		}
		
		offset = 1;
		length = 16;
		for (int dir = 0; dir < 4; dir++){
			keyFrames = new Array<Sprite>();
			for (int i = offset; i < offset+length; i++){
				Sprite s = atlas.createSprite("bat", i+dir*period);
				s.setSize(4f, 4f);
				if (s == null) throw new GdxRuntimeException("sdjkl "+i);
				keyFrames.add(s);
			}
			Animation anim = new Animation(.02f, keyFrames);
			animations[BAT_ID][progress++] = anim;
		}
		
		offset = 1;
		length = 16;
		for (int dir = 0; dir < 4; dir++){
			keyFrames = new Array<Sprite>();
			for (int i = offset; i < offset+length; i++){
				Sprite s = atlas.createSprite("bat", i+dir*period);
				s.setSize(4f, 4f);
				if (s == null) throw new GdxRuntimeException("sdjkl "+i);
				keyFrames.add(s);
			}
			Animation anim = new Animation(.02f, keyFrames);
			animations[BAT_ID][progress++] = anim;
		}
		
		offset = 1;
		length = 16;
		for (int dir = 0; dir < 4; dir++){
			keyFrames = new Array<Sprite>();
			for (int i = offset; i < offset+length; i++){
				Sprite s = atlas.createSprite("bat", i+dir*period);
				s.setSize(4f, 4f);
				if (s == null) throw new GdxRuntimeException("sdjkl "+i);
				keyFrames.add(s);
			}
			Animation anim = new Animation(.02f, keyFrames);
			animations[BAT_ID][progress++] = anim;
		}
		offset = 1;
		length = 16;
		for (int dir = 0; dir < 4; dir++){
			keyFrames = new Array<Sprite>();
			for (int i = offset; i < offset+length; i++){
				Sprite s = atlas.createSprite("bat", i+dir*period);
				s.setSize(4f, 4f);
				if (s == null) throw new GdxRuntimeException("sdjkl "+i);
				keyFrames.add(s);
			}
			Animation anim = new Animation(.02f, keyFrames);
			animations[BAT_ID][progress++] = anim;
		}
		
	}

}
