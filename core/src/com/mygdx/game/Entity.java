package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasSprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pools;
import com.mygdx.game.entities.Player;

public abstract class Entity {
	public Vec3i mapPosition = new Vec3i(), moveToPosition = new Vec3i();
	public float animTime, moveTime;
	public float health, maxHealth;
	public int turns, direction, animID = 0;
	public Vector2 position = new Vector2(9,9), moveToPositionV = new Vector2(13, 13);
	protected float maxMoveTime = .21f;
	public boolean finishedMoving;
	public int moves = 1;
	public int animNameID = 0;
	protected boolean needsToFinishAnim = true;
	protected boolean leaveAnimDelta;
	
	public boolean extraHealth = false;
	
	public void draw(SpriteBatch batch, float delta){
		
		animTime += delta;
		Animation anim = Animations.animations[animNameID][animID*4+direction];
		//Gdx.app.log("", "anim"+animID+"  dir "+direction + "  tot"+(animID+direction));
		AtlasSprite s = (AtlasSprite) anim.getKeyFrame(animTime, true);
		//batch.draw(s, position.x, position.y+1, 1f, 1f);
		s.setPosition(position.x-1.5f,  position.y-.5f);
		s.draw(batch);
		//batch.dr
	};
		
	
	
	
	public abstract boolean strategize(Map map, EntityGroup enemies, EntityGroup allies);
	public boolean lowHealth = false, takenDamage = false;
	public void takeDamage(float damage, Map map){
		if (takenDamage) return;
		if (lowHealth || !extraHealth) die();
		else lowHealth = true;
		takenDamage = true;
		
	}


	public abstract void onSpawn();

	private void die() {
		Gdx.app.log("G", "take damage"+lowHealth);
		animID = Animations.DIE;
		animTime = 0f;
		moveTime = 0f;
		this.finishedMoving = false;
		//this.needsToFinishAnim = true;
		this.maxMoveTime = .1f;
		//lowHealth = false;
		moves = 0;
	}



	public boolean isPlayer = false;
	public void move(float delta, EntityGroup allies, Map map, EntityGroup items, EntityGroup enemies) {
		if (finishedMoving) return;
		moveTime += delta;
		position.set(mapPosition.x, mapPosition.y);
		moveToPositionV.set(moveToPosition.x, moveToPosition.y);
		//moveToPosition.x = 13;
		//moveToPosition.y = 13;;
		float factor = 1f;
		factor = moveTime / maxMoveTime;
		if (factor > 1f && (!needsToFinishAnim  || animTime > Animations.animations[animNameID][animID*4+direction].getAnimationDuration())){
			//IF THROWING DO IT HERE
			if (isThrowing() )
				doThrow(allies);
			//Gdx.app.log("2jj", "movement alpha"+factor);
			finishedMoving = true;
			mapPosition.set(moveToPosition);
			onFinishedMoving(map, allies, items, enemies);
			if (animID == Animations.DIE){
				if (isPlayer){
					items.removeAll();
					enemies.removeAll();
					allies.removeAll();
					Gdx.app.log("2jj", "RESET"+factor);
					map.resetMap((Player) this, allies, enemies, map);
				}
				Pools.free(allies.removeValue(this, true));
			}
			if (!leaveAnimDelta)animID = Animations.STAND;
		
			
		}
		factor = Math.min(1f,  factor);
		
		position.lerp(moveToPositionV, factor);
		//Gdx.app.log("g", "move"+moveTime);
		//return finishedMoving;
	}



	public abstract void onFinishedMoving(Map map, EntityGroup allies, EntityGroup items, EntityGroup enemies);




	public void doThrow(EntityGroup allies) {
		
		
	}




	public boolean isThrowing() {
		// TODO Auto-generated method stub
		return false;
	}



	public static Vector2 tmpV = new Vector2();
	public boolean tryToMove(int direction, Map map, EntityGroup enemies) {
		
		//Gdx.app.log("", "jklds"+moves);
		//if (!finishedMoving) return true;
		switch (direction){
		case Main.UP:
			tmpV.set(0,1);
			break;
		case Main.DOWN:
			tmpV.set(0,-1);
			break;
		case Main.LEFT:
			tmpV.set(-1,0);
			break;
		case Main.RIGHT:
			tmpV.set(1,0);
			break;
		}
		tmpV.x += mapPosition.x;
		tmpV.y += mapPosition.y;
		this.direction = direction;
		if (map.isEmpty(tmpV.x, tmpV.y)){
			Array<Entity> arr = enemies.getForPosition(tmpV);
			if (arr.size != 0) {
				//throw new GdxRuntimeException("");
				//ATTACKING
				//melee(arr);
				for (int i = 0; i < arr.size; i++){
					arr.get(i).takeDamage(1f, map);
				}
				animID = Animations.ATTACK;
			} else {
				moveToPosition.set(tmpV.x, tmpV.y, 0);
				animID = Animations.WALK;
			}
			
			moveTime = 0;
			finishedMoving = false;
			moves--;
			
			animTime = 0f;
			return true;
		};
		return false;
		
		
	}




	public abstract void onStartMove();
		
		
	




	
	





}
