package com.mygdx.game;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pools;

public class EntityGroup extends Array<Entity> {

	public boolean finishedMoving;
	public boolean needsToStrategize;

	public void draw(SpriteBatch batch, float delta) {
		for (int i = 0; i < size; i++){
			Entity e = get(i);
			e.draw(batch, delta);
		}
		
	}
	
	public boolean shouldRetryStrategizing(){
		for (int i = 0; i < size; i++){
			Entity e = get(i);
			
			if (e.moves > 0)
				return true;;
			
		
		}
		return false;
	}
	
	public void strategize(com.mygdx.game.Map map, EntityGroup enemies, EntityGroup allies) {
		
		needsToStrategize = false;
		
		for (int i = 0; i < size; i++){
			Entity e = get(i);
			if (e.moves > 0){
				
				//dif (e.finishedMoving)
					e.strategize(map, enemies, allies);
			}
			if (e.moves > 0)
				needsToStrategize = true;
			
		}
		
	}
	
	
	
	public void onNewMove(){
		//Gdx.app.log("gr", "new mv");
		for (int i = 0; i < size; i++){
			Entity e = get(i);
			e.finishedMoving = false;
			e.moveTime = 0f;
			e.animTime = 0f;
			e.onStartMove();
			if (!e.takenDamage) e.lowHealth = false;
			e.takenDamage = false;
			
		}
		finishedMoving = false;
	}

	public void move(float delta, EntityGroup allies, Map map, EntityGroup items, EntityGroup enemies) {//returns true if its done with the moving stage.
		finishedMoving = true;
		for (int i = 0; i < size; i++){
			Entity e = get(i);
			if (!e.finishedMoving)
				e.move(delta, allies, map, items, enemies);
			if (!e.finishedMoving){
				finishedMoving = false;
			}
		}
		
		
	}
	private Array<Entity> arr = new Array<Entity>();
	public Array<Entity> getForPosition(Vector2 pos) {
		// TODO Auto-generated method stub
		arr.clear();
		for (int i = 0; i < size; i++){
			Entity e = get(i);
			int x = (int) pos.x;
			int y = (int) pos.y;
			if (e.mapPosition.equals(x, y, 0))
				arr.add(e);
		}
		return arr;
		
	}

	public Array<Entity> closestEntity(Entity to) {
		arr.clear();
		float dist = 10000000;
		for (int i = 0; i < size; i++){
			Entity e = get(i);
			int x = (int) to.mapPosition.x;
			int y = (int) to.mapPosition.y;
			
			if (e.mapPosition.dst(x,y) < dist){
				arr.clear();
				arr.add(e);
				dist = e.mapPosition.dst(x,y);
			}
		}
		return arr;
	}

	public void removeAll() {
		while (size > 0)Pools.free(pop());
		
	}

	

}
