package com.mygdx.game;

import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

public class InvButton extends ImageButton {
	public Label label;
	public InvButton(Skin skin) {
		super(skin);
		label = new Label("2", skin);
		this.add(label);
	}

}
