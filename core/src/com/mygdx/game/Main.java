package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ButtonGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener.ChangeEvent;
import com.mygdx.game.entities.Player;

public class Main extends ApplicationAdapter {
	public static final int INV_SLOTS = 6;
	private static final int STATE_PLAYER_STRATEGIZE = 0;
	private static final int STATE_PLAYER_ANIMATE = 1;
	private static final int STATE_ENEMY_STRATEGIZE = 2;
	private static final int STATE_ENEMY_ANIMATE = 3;
	private static final int STATE_WAIT_FOR_PLAYER = 4;
	public static final float ANIMATE_TIME = 1f;
	public static final int LEFT = 1;
	public static final int RIGHT = 3;
	public static final int UP = 2;
	public static final int DOWN = 0;
	SpriteBatch batch;
	Texture img;
	Stage stage;
	Skin skin;
	private int state = 0;
	private Table invTable;
	private EntityGroup playerEntities, enemyEntities, itemEntities;
	OrthographicCamera camera;
	private float viewportWidth = 14;
	private float viewportHeight = 10;
	private Map map;
	private OrthographicCamera uiCamera;
	private Player player;
	boolean waitingForPlayer = true;
	private InvButton[] invButtons;
	private BitmapFont font;
	
	@Override
	public void create () {
		float ar = (float)Gdx.graphics.getWidth()/(float)Gdx.graphics.getHeight();
		viewportWidth = 14f;
		viewportHeight = 14f/ar;;
		invButtons = new InvButton[INV_SLOTS];
		batch = new SpriteBatch();
		img = new Texture("badlogic.jpg");
		stage = new Stage();
		skin = new Skin();
		TextureAtlas uiAtlas = new TextureAtlas("ui.atlas");
		TextureAtlas atlas = new TextureAtlas("ld32.pack");
		TextureAtlas itemAtlas = new TextureAtlas("items.pack");
		TextureAtlas particleAtlas = new TextureAtlas("ld32particles.pack");
		Texture normals = new Texture("ld32normals.png");
		font = SkinUtil.makeSkin(skin, uiAtlas, itemAtlas);
		
		
		
		
		playerEntities = new EntityGroup();
		enemyEntities = new EntityGroup();
		itemEntities = new EntityGroup();
		camera = new OrthographicCamera(viewportWidth, viewportHeight);
		camera.translate(10, 10);
		camera.update();
		uiCamera = (OrthographicCamera) stage.getCamera();
		player = new Player(invGroup);
		map = new Map(atlas, normals, enemyEntities, particleAtlas);
		
		state = STATE_WAIT_FOR_PLAYER;
		map.setParticle(10, 10);;
		
		invTable = new Table(skin);
		invTable.setFillParent(true);
		Table insideTable = createInvTable(new Table(skin));
		invTable.add(insideTable).top();
		invTable.row();
		invTable.add(lowHealthLabel);
		invTable.row();
		invTable.add(new Table()).expandY();
		Animations.init(atlas);
		updateInventoryButtons();
		
		stage.addActor(invTable);
		Gdx.input.setInputProcessor(stage);
		map.resetMap(player, playerEntities, enemyEntities, map);
		map.items = itemEntities;
		
	}
	private boolean lowHealthOn = true;
	
	private void updateInventoryButtons(){
		for (int i = 0; i < INV_SLOTS; i++){
			invButtons[i].setStyle(SkinUtil.buttonStyles[player.inv[i]]);
			invButtons[i].label.setText(""+player.amounts[i]);
			//invButtons[i].getStyle().up = SkinUtil.itemSprites[player.inv[i]];
			//invButtons[i].getStyle().down = SkinUtil.itemSprites[player.inv[i]];
			//invButtons[i].getStyle().checked = SkinUtil.itemSprites[player.inv[i]];
			//invButtons[i].getImage().setFillParent(true);
		}
		
	}

	ButtonGroup<InvButton> invGroup = new ButtonGroup<InvButton>();
	private Label lowHealthLabel;
	public static boolean queueInvUpdate;
	private Table createInvTable(Table table) {
		final Main main = this;;
		for (int i = 0; i < INV_SLOTS; i++){
			InvButton b = new InvButton(skin);
			b.addListener(new EventListener(){

				@Override
				public boolean handle(Event event) {
					if (event instanceof ChangeEvent && player != null){
						ChangeEvent ch = (ChangeEvent) event;
						
						int in = player.inv[player.getActiveInv()];
						player.animNameID = in+6;
					}
					return false;
				}
				
			});
			table.add(b);
			invButtons[i] = b;
			invGroup.add(b);
		}
		invButtons[0].setChecked(true);
		lowHealthLabel = new Label("LOW HEALTH", skin);
		
		return table;
	}

	@Override
	public void render () {
		if (!playerEntities.contains(player, true)){
			playerEntities.add(player);;
			map.resetMap(player, playerEntities, enemyEntities, map);
		}
		if (queueInvUpdate){
			queueInvUpdate = false;
			updateInventoryButtons();
		}
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		float delta = Gdx.graphics.getDeltaTime();
	//Gdx.app.log("main",  "state "+state+"  player"+player.animTime+"  "+player.moveTime + player.finishedMoving);;
		switch (state){
		case STATE_WAIT_FOR_PLAYER:
			
			if (Gdx.input.isKeyPressed(Keys.A) || Gdx.input.isKeyPressed(Keys.LEFT)){
				if (Gdx.input.isKeyPressed(Keys.SHIFT_RIGHT) || Gdx.input.isKeyPressed(Keys.SHIFT_LEFT)){
					if (player.tryToThrow(LEFT, map, playerEntities)){
						state = STATE_PLAYER_STRATEGIZE;
						//enemyEntities.onNewMove();
					}
				} else {
					
					if (player.tryToMove(LEFT, map, enemyEntities))
						state = STATE_PLAYER_STRATEGIZE;
				}
				waitingForPlayer = false;
			}
			if (Gdx.input.isKeyPressed(Keys.S) || Gdx.input.isKeyPressed(Keys.DOWN)){
				if (Gdx.input.isKeyPressed(Keys.SHIFT_RIGHT) || Gdx.input.isKeyPressed(Keys.SHIFT_LEFT)){
					if (player.tryToThrow(DOWN, map, playerEntities)){
						state = STATE_PLAYER_STRATEGIZE;
						//enemyEntities.onNewMove();
					}
				} else {
					if (player.tryToMove(DOWN, map, enemyEntities))
						state = STATE_PLAYER_STRATEGIZE;
				}
				waitingForPlayer = false;
			}
			if (Gdx.input.isKeyPressed(Keys.D) || Gdx.input.isKeyPressed(Keys.RIGHT)){
				if (Gdx.input.isKeyPressed(Keys.SHIFT_RIGHT) || Gdx.input.isKeyPressed(Keys.SHIFT_LEFT)){
					if (player.tryToThrow(RIGHT, map, playerEntities)){
						state = STATE_PLAYER_STRATEGIZE;
						//enemyEntities.onNewMove();
					}
				} else {
					if (player.tryToMove(RIGHT, map, enemyEntities))
						state = STATE_PLAYER_STRATEGIZE;
				}
				waitingForPlayer = false;
			}
			if (Gdx.input.isKeyPressed(Keys.W) || Gdx.input.isKeyPressed(Keys.UP)){
				if (Gdx.input.isKeyPressed(Keys.SHIFT_RIGHT) || Gdx.input.isKeyPressed(Keys.SHIFT_LEFT)){
					if (player.tryToThrow(UP, map, playerEntities)){
						state = STATE_PLAYER_STRATEGIZE;
						//enemyEntities.onNewMove();
					}
				} else {
					if (player.tryToMove(UP, map, enemyEntities))
						state = STATE_PLAYER_STRATEGIZE;
				}
				waitingForPlayer = false;
			}
			
			if (Gdx.input.isKeyPressed(Keys.NUM_1)){
				invButtons[0].setChecked(true);
				if (Gdx.input.isKeyPressed(Keys.SHIFT_RIGHT) || Gdx.input.isKeyPressed(Keys.SHIFT_LEFT))
					player.useSelf();	
				waitingForPlayer = false;
			}
			if (Gdx.input.isKeyPressed(Keys.NUM_2)){
				invButtons[1].setChecked(true);
				if (Gdx.input.isKeyPressed(Keys.SHIFT_RIGHT) || Gdx.input.isKeyPressed(Keys.SHIFT_LEFT))
					player.useSelf();	
				waitingForPlayer = false;
			}
			if (Gdx.input.isKeyPressed(Keys.NUM_3)){
				invButtons[2].setChecked(true);
				if (Gdx.input.isKeyPressed(Keys.SHIFT_RIGHT) || Gdx.input.isKeyPressed(Keys.SHIFT_LEFT))
					player.useSelf();	
				waitingForPlayer = false;
			}
			if (Gdx.input.isKeyPressed(Keys.NUM_4)){
				invButtons[3].setChecked(true);
				if (Gdx.input.isKeyPressed(Keys.SHIFT_RIGHT) || Gdx.input.isKeyPressed(Keys.SHIFT_LEFT))
					player.useSelf();	
				waitingForPlayer = false;
			}
			if (Gdx.input.isKeyPressed(Keys.NUM_5)){
				invButtons[4].setChecked(true);
				if (Gdx.input.isKeyPressed(Keys.SHIFT_RIGHT) || Gdx.input.isKeyPressed(Keys.SHIFT_LEFT))
					player.useSelf();	
				waitingForPlayer = false;
			}
			if (Gdx.input.isKeyPressed(Keys.NUM_6)){
				invButtons[5].setChecked(true);
				if (Gdx.input.isKeyPressed(Keys.SHIFT_RIGHT) || Gdx.input.isKeyPressed(Keys.SHIFT_LEFT))
					player.useSelf();	
				waitingForPlayer = false;
			}
			
			
			if (state == STATE_PLAYER_STRATEGIZE)
				playerEntities.onNewMove();
			break;
		case STATE_PLAYER_STRATEGIZE:
			
			playerEntities.strategize(map, enemyEntities, playerEntities);
			state = STATE_PLAYER_ANIMATE;
			//if (playerEntities.state = STATE_ENEMY_STRATEGIZE;
			//playerEntities.move(delta, playerEntities);
			break;
		case STATE_PLAYER_ANIMATE:
			//Gdx.app.log("main",  "state "+state+"  player"+player.animTime+"  "+player.moveTime + player.finishedMoving);;

			playerEntities.move(delta, playerEntities, map, itemEntities, enemyEntities);
			if (playerEntities.finishedMoving){
				
				playerEntities.finishedMoving = false;
				if (playerEntities.shouldRetryStrategizing()){
					//Gdx.app.log("main", "STRAT"+player.moves);
					state = STATE_PLAYER_STRATEGIZE;
					
					//playerEntities.resetFinishedMoving();
					
				}
				else{
					state = STATE_ENEMY_STRATEGIZE;
					enemyEntities.onNewMove();
				}
				
			}
			break;
		case STATE_ENEMY_STRATEGIZE:
			//Gdx.app.log("main", "enemy strat"+enemyEntities.get(0).moves);
			
			enemyEntities.strategize(map, playerEntities, enemyEntities);
			state = STATE_ENEMY_ANIMATE;
			
			break;
		case STATE_ENEMY_ANIMATE:
			//Gdx.app.log("main", "enemy anim");

			enemyEntities.move(delta, enemyEntities, map, itemEntities, playerEntities);
			if (enemyEntities.finishedMoving){
				enemyEntities.finishedMoving = false;
				if (enemyEntities.shouldRetryStrategizing()){
					//Gdx.app.log("main", "STRAT"+player.moves);
					state = STATE_ENEMY_STRATEGIZE;
					
					//playerEntities.resetFinishedMoving();
					
				} else {
					state = STATE_WAIT_FOR_PLAYER;
					//Gdx.app.log("main", "end of loop");
					map.updateParticles(enemyEntities, playerEntities);
					waitingForPlayer = true;
				}
				
			}
			
		}
		
		
		
		camera.position.set(player.position.x+.5f, player.position.y+2, 0);
		camera.update();
		batch.setProjectionMatrix(camera.combined);
		batch.begin();
		//batch.draw(img, 0, 0);
		map.draw(batch, camera);
		playerEntities.draw(batch, delta);
		enemyEntities.draw(batch, delta);
		itemEntities.draw(batch, delta);
		batch.end();
		
		
		batch.setProjectionMatrix(uiCamera.combined);
		batch.begin();
		if (player.lowHealth){
			if (!lowHealthOn){
				invTable.add(lowHealthLabel);
				lowHealthOn = true;
				//Gdx.app.log("jj", "low on");
			} else {
				
			}
				//font.draw(batch, "lowsdffdsfddsffdssdfdsdfssdfsdf", 200, 200);
				//Gdx.app.log("jj", "low");
		} else if (lowHealthOn){
			lowHealthOn = false;
			invTable.removeActor(lowHealthLabel);
			//Gdx.app.log("jj", "low off");
		}
		//stage.setDebugAll(true);
		stage.draw();
		batch.end();
	}
}
