package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasSprite;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.IntArray;
import com.badlogic.gdx.utils.Pools;
import com.mygdx.game.entities.Bat;
import com.mygdx.game.entities.Item;
import com.mygdx.game.entities.Player;
import com.mygdx.game.entities.Wizard;

public class Map {
private static final int WIDTH = 128;
private static final int HEIGHT = 128;
private static final int MAX_SPRITES = 1024, DRAW_MASK = 0x00fff, EMPTY_LIMIT = 512, PARTICLE_START = EMPTY_LIMIT+64
, DATA_MASK = 0x00fff;

public static final float DEFAULT_LIGHT_Z = 0.125f;
public static final float AMBIENT_INTENSITY = 0.3f;
public static final float LIGHT_INTENSITY = 1f;

public static final Vector3 LIGHT_POS = new Vector3(.5f,.5f,DEFAULT_LIGHT_Z);

//Light RGB and intensity (alpha)
public static final Vector3 LIGHT_COLOR = new Vector3(1f, 0.8f, 0.6f);

//Ambient RGB and intensity (alpha)
public static final Vector3 AMBIENT_COLOR = new Vector3(1f, 1f, 1f);

//Attenuation coefficients for light falloff
public static final Vector3 FALLOFF = new Vector3(.4f, 3f, 20f);
private static final int DARK_LIMIT = 1000;


public int[] tiles = new int[WIDTH*HEIGHT], particles = new int[WIDTH*HEIGHT];;

public AtlasSprite[] tileSprites = new AtlasSprite[MAX_SPRITES];

private Vector3 proja = new Vector3(), projb = new Vector3();
private ShaderProgram shader;
private Texture normals;
private Texture diffuse;
public static final int[] PARTICLE_NUMBERS = {0, 10, 23, 32};


public Map(TextureAtlas atlas, Texture normals, EntityGroup enemies, TextureAtlas particleAtlas){
	updateOrder = new IntArray();
	for (int x = 2; x < WIDTH-3; x++)
		for (int y = 2; y < HEIGHT-3; y++){
			updateOrder.add(x+y*WIDTH);
		}
	updateOrder.shuffle();
	String name;
	shader = new ShaderProgram(Gdx.files.internal("lighting.vert"), Gdx.files.internal("lighting.frag"));
	shader.begin();
	shader.setUniformf("LightColor[0]", LIGHT_COLOR.x, LIGHT_COLOR.y, LIGHT_COLOR.z, LIGHT_INTENSITY);
	shader.setUniformf("AmbientColor", AMBIENT_COLOR.x, AMBIENT_COLOR.y, AMBIENT_COLOR.z, AMBIENT_INTENSITY);
	shader.setUniformf("Falloff[0]", FALLOFF);
	//our normal map
	shader.setUniformi("u_normals", 1); //GL_T
	shader.setUniformf("Resolution", Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
	shader.end();
	this.normals = normals;
	this.diffuse = atlas.getTextures().first();
	name = "floor";
	for (int i = 0; i < 64; i++){
		tileSprites[i] = (AtlasSprite) atlas.createSprite(name, i+1);
		tileSprites[i].setSize(1f,  2f);
		
	}
	
	name = "wall";
	for (int i = 0; i < 64; i++){
		tileSprites[i+EMPTY_LIMIT] = (AtlasSprite) atlas.createSprite(name, i+1);
		tileSprites[i+EMPTY_LIMIT].setSize(1f,  2f);
		//if (tileSprites[i+64] == null) throw new GdxRuntimeException("jkl");
	}
	
	int prog = 0;
	
	name = "CROSS";
	for (int p = 0; p < 4; p++)
	for (int i = 0; i < PARTICLE_NUMBERS[p]; i++){
		tileSprites[PARTICLE_START+prog] = (AtlasSprite) particleAtlas.createSprite(name+p, i+1);
		//if (tileSprites[i+PARTICLE_START+prog] == null) throw new GdxRuntimeException("jkl "+(i) + " "+(name+p));
		tileSprites[PARTICLE_START+prog].setSize(1f,  2f);
		prog++;
	}
	prog = 0;
	
	name = "GAS";
	for (int p = 0; p < 4; p++)
		for (int i = 0; i < PARTICLE_NUMBERS[p]; i++){
		tileSprites[PARTICLE_START+64+prog] = (AtlasSprite) particleAtlas.createSprite(name+p, i+1);
		tileSprites[PARTICLE_START+64+prog].setSize(1f,  2f);
		//if (tileSprites[i+64] == null) throw new GdxRuntimeException("jkl");
		prog++;
	}
	Gdx.app.log("d", "DONE"+prog);
	prog = 0;
	name = "FIRE";
	for (int p = 0; p < 4; p++)
		for (int i = 0; i < PARTICLE_NUMBERS[p]; i++){
		tileSprites[PARTICLE_START+128+prog] = (AtlasSprite) particleAtlas.createSprite(name+p, i+1);
		tileSprites[PARTICLE_START+128+prog].setSize(1f,  2f);
		//if (tileSprites[i+64] == null) throw new GdxRuntimeException("jkl");
		prog++;
	}
	
	for (int i = 0; i < WIDTH*HEIGHT; i++)
		tiles[i] = MathUtils.random(1, 61);
	
	for (int i = 0; i < WIDTH; i++)
		tiles[i+WIDTH*10] = EMPTY_LIMIT+MathUtils.random(63);
	
	
}

	private void generateCaves() {
		
		for (int x = 0; x < WIDTH; x++)
			tiles[x] = EMPTY_LIMIT;
		for (int x = 0; x < WIDTH; x++)
			for (int y = 1; y < HEIGHT; y++){
				float px = x/6f, py = y/6f;
				boolean solid = Math.abs(SimplexNoise.noise(px, py, 5)) < .432f;
				if (!solid){
					
					tiles[x+y*WIDTH] = EMPTY_LIMIT+MathUtils.random(63);
				} else {
					tiles[x+y*WIDTH] = MathUtils.random(63);
				}
				
			}
		
		for (int x = 1; x < WIDTH-1; x++)
			for (int y = 1; y < HEIGHT-1; y++){
				if (!isEmpty(x,y) && !
						(//isEmpty(x-1, y) || isEmpty(x+1, y) ||
								isEmpty(x, y-1) 
								//|| isEmpty(x-1, y+1
										//)
										))
				tiles[x+y*WIDTH] = DARK_LIMIT;
			}
		
	}
		
	public Vector2 startPosition = new Vector2();
	public void findStartPosition(){
		for (int i = 0; i < 50; i++){
			int x = MathUtils.random(2, WIDTH-3);
			int y = MathUtils.random(2, HEIGHT-3);
			if (isEmpty(x,y)){
				startPosition.set(x,y);
				return;
			}
		}
	}

	public void generateBats(EntityGroup group){
		for (int i = 0; i < 50; i++){
			Bat bat = Pools.obtain(Bat.class);
			bat.onSpawn();
			int x = MathUtils.random(WIDTH-2), y = MathUtils.random(1, HEIGHT-2);
			bat.mapPosition.set(x,y,0);
			bat.moveToPosition.set(x,y,0);
			group.add(bat);
		}
	}
	
	public void generateWizards(EntityGroup group){
		for (int i = 0; i < 50; i++){
			Wizard bat = Pools.obtain(Wizard.class);
			bat.onSpawn();
			int x = MathUtils.random(WIDTH-2), y = MathUtils.random(1, HEIGHT-2);
			bat.mapPosition.set(x,y,0);
			bat.moveToPosition.set(x,y,0);
			group.add(bat);
		}
	}
	
	public void updateGenerateBats(EntityGroup group){
		for (int i = 0; i < 5; i++){
			Bat bat = Pools.obtain(Bat.class);
			bat.onSpawn();
			int x = MathUtils.random(WIDTH-2), y = MathUtils.random(1, HEIGHT-2);
			bat.mapPosition.set(x,y,0);
			bat.moveToPosition.set(x,y,0);
			group.add(bat);
		}
	}
	
	public void updateGenerateWizards(EntityGroup group){
		for (int i = 0; i < 5; i++){
			Wizard bat = Pools.obtain(Wizard.class);
			bat.onSpawn();
			int x = MathUtils.random(WIDTH-2), y = MathUtils.random(1, HEIGHT-2);
			bat.mapPosition.set(x,y,0);
			bat.moveToPosition.set(x,y,0);
			group.add(bat);
		}
	}

	public void setParticle(int x, int y){
		particles[x+y*WIDTH] = PARTICLE_START+1;
	}

	public void draw(SpriteBatch batch, Camera cam){
		batch.end();
		
		
		batch.setShader(shader);
		
		
		batch.disableBlending();
		batch.begin();
		shader.setUniformf("LightPos[0]", LIGHT_POS);
		normals.bind(1);
		diffuse.bind(0);
		
		proja.set(0,0, 0);
		projb.set(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), 0);
		cam.unproject(proja);
		cam.unproject(projb);
		
		if (proja.x > projb.x){
			float t = proja.x;
			proja.x = projb.x;
			projb.x = t;
		}
		if (proja.y > projb.y){
			float t = proja.y;
			proja.y = projb.y;
			projb.y = t;
		}
		
		int x0 = (int) proja.x, y0 = (int) proja.y-1, x1 = (int) projb.x, y1 = (int) projb.y;
		x0 = Math.min(Math.max(0,  x0), WIDTH-1);
		x1 = Math.min(Math.max(0,  x1), WIDTH-1);
		y0 = Math.min(Math.max(0,  y0), HEIGHT-1);
		y1 = Math.min(Math.max(0,  y1), HEIGHT-2);
		
		for (int x = x0; x <= x1; x++){
			for (int y = y0; y <= y1+1; y++){
				int tile = tiles[x+y*WIDTH];
				int draw = tile & DRAW_MASK;
				AtlasSprite s = null;
				s = tileSprites[draw];
				//Gdx.app.log("d", "draw"+draw);
				if (tile < EMPTY_LIMIT){
					s.setPosition(x, y+1);
				} else {
					if (tile >= DARK_LIMIT){continue;}
					else s.setPosition(x, y);
				}
				s.draw(batch);
			}
		}
		
		batch.end();
		batch.enableBlending();
		batch.setShader(null);
		batch.begin();
		for (int x = x0; x <= x1; x++){
			for (int y = y0; y <= y1+1; y++){
				int tile = particles[x+y*WIDTH];
				int draw = tile & DRAW_MASK;
				AtlasSprite s = null;
				s = tileSprites[draw];
				
				if (tile < EMPTY_LIMIT){
					continue;//s.setPosition(x, y);
				} else {
					s.setPosition(x, y);
				}
				s.draw(batch);
			}
		}
	}
	IntArray updateOrder;
	public void updateParticles(EntityGroup enemies, EntityGroup allies){
		for (int i = 0; i < updateOrder.size; i++){
			particle(updateOrder.get(i));
		}
		updateGenerateBats(enemies);
		updateGenerateWizards(enemies);
	}
	public void particle(int index){
		int x = index % WIDTH;
		int y = index / WIDTH;
		int tile = particles[index];
		if (tile < PARTICLE_START) return;
		tile -= PARTICLE_START;
		if (tile < 64){
			Gdx.app.log("map2,", "particles");
			int l = particles[ (x-1)+(y)*WIDTH]-PARTICLE_START;
			int t = particles[ (x)+(y+1)*WIDTH]-PARTICLE_START;
			int b = particles[ (x)+(y-1)*WIDTH]-PARTICLE_START;
			int r = particles[ (x+1)+(y)*WIDTH]-PARTICLE_START;
			
			if (tile < 3) {
				particles[index] = 0;
				return;
			}
			if (l >= 0 && l <64){
				int tot = (l + tile)/2;
				particles[ (x-1)+(y)*WIDTH] = tot+PARTICLE_START;
				tile = tot;
			}
			if (l < 0 && isEmpty(x-1,y)){
				
				particles[ (x-1)+(y)*WIDTH] = Math.max(0, (tile/2))+PARTICLE_START;
				tile = (tile/2) ;
			}
			if (l >= 64){
				particles[ (x-1)+(y)*WIDTH] = 63+PARTICLE_START;
			}
			
			if (r >= 0 && r <64){
				int tot = (r + tile)/2;
				particles[ (x+1)+(y)*WIDTH] = tot+PARTICLE_START;
				tile = tot;
			}
			if (r < 0 && isEmpty(x+1,y)){
				particles[ (x+1)+(y)*WIDTH] = Math.max(0, (tile/2-1))+PARTICLE_START;
				tile = (tile/2) ;
			}
			if (r >= 64){
				particles[ (x+1)+(y)*WIDTH] = 63+PARTICLE_START;
			}
			
			if (t >= 0 && t <64){
				int tot = (t + tile)/2;
				particles[ (x)+(y+1)*WIDTH] = tot+PARTICLE_START;
				tile = tot;
			}
			if (t < 0 && isEmpty(x,y+1)){
				particles[ (x)+(y+1)*WIDTH] = Math.max(0, (tile/2))+PARTICLE_START;
				tile = (tile/2) ;
			}
			if (t >= 64){
				particles[ (x)+(y+1)*WIDTH] = 63+PARTICLE_START;
			}
			
			if (b >= 0 && b <64){
				int tot = (b + tile)/2;
				particles[ (x)+(y-1)*WIDTH] = tot+PARTICLE_START;
				tile = tot;
			}
			if (b < 0 && isEmpty(x,y-1)){
				
				particles[ (x)+(y-1)*WIDTH] = Math.max(0, (tile/2))+PARTICLE_START;
				tile = (tile/2) ;
			}
			if (b >= 64){
				particles[ (x)+(y-1)*WIDTH] = 63+PARTICLE_START;
			}
			//tile -= 1;
			if (tile >= 0)
				particles[index] = tile + PARTICLE_START;
			
			else particles[index] = 0;
		} else if (tile < 128){//gas
			tile -= 64;
			int l = particles[ (x-1)+(y)*WIDTH]-PARTICLE_START;
			int t = particles[ (x)+(y+1)*WIDTH]-PARTICLE_START;
			int b = particles[ (x)+(y-1)*WIDTH]-PARTICLE_START;
			int r = particles[ (x+1)+(y)*WIDTH]-PARTICLE_START;
			
			switch (MathUtils.random(3)){
			case 0:
				if (l >= 0 && l <64){
					int tot = (l + tile)/2;
					particles[ (x-1)+(y)*WIDTH] = tot+PARTICLE_START+64;
					tile = tot;
				}
				if (l < 0 && isEmpty(x-1,y)){
					
					particles[ (x-1)+(y)*WIDTH] = Math.max(0, (tile/2))+PARTICLE_START+64;
					tile = (tile-1) ;
				}
				break;
			case 1:
				
				if (r >= 0 && r <64){
					int tot = (r + tile)/2;
					particles[ (x+1)+(y)*WIDTH] = tot+PARTICLE_START+64;
					tile = tot;
				}
				if (r < 0 && isEmpty(x+1,y)){
					particles[ (x+1)+(y)*WIDTH] = Math.max(0, (tile/2-1))+PARTICLE_START+64;
					tile = (tile-1) ;
				}
				break;
			case 2:
				
				if (t >= 0 && t <64){
					int tot = (t + tile)/2;
					particles[ (x)+(y+1)*WIDTH] = tot+PARTICLE_START+64;
					tile = tot;
				}
				if (t < 0 && isEmpty(x,y+1)){
					particles[ (x)+(y+1)*WIDTH] = Math.max(0, (tile/2))+PARTICLE_START+64;
					tile = (tile-1) ;
				}
				break;
			case 3:
			
			if (b >= 0 && b <64){
				int tot = (b + tile)/2;
				particles[ (x)+(y-1)*WIDTH] = tot+PARTICLE_START+64;
				tile = tot;
			}
			if (b < 0 && isEmpty(x,y-1)){
				
				particles[ (x)+(y-1)*WIDTH] = Math.max(0, (tile/2))+PARTICLE_START+64;
				tile = (tile-1) ;
			}
			
			break;
			}
			
			if (tile < 3) {
				particles[index] = 0;
				return;
			}
			
			
			
			//tile -= 1;
			if (tile >= 0)
				particles[index] = tile + PARTICLE_START+64;
			
			else particles[index] = 0;
			
		} else {
			tile -= 128;
			for (int i = 0; i < 4; i++){
				int dx = MathUtils.random(2)-1;
				int dy = MathUtils.random(2)-1;
				int look = particles[ (x+dx)+(y+dy)*WIDTH]-PARTICLE_START-128;
				
				
				if (tile < 3) {
					particles[index] = 0;
					return;
				}
				
				if (look >= 0 && look <64){
					int tot = (look + tile)/2;
					particles[ (x+dx)+(y+dy)*WIDTH] = tot+PARTICLE_START+128;
					tile = tot;
				}
				if (look < 0 && isEmpty(x+dx,y+dy)){
					
					particles[ (x+dx)+(y+dy)*WIDTH] = Math.max(0, (tile/2))+PARTICLE_START+128;
					tile = (tile-1) ;
				}
				
				
				
				
				//tile -= 1;
				if (tile >= 0)
					particles[index] = tile + PARTICLE_START+128;
				
				else particles[index] = 0;
			}
			
			
		}
		
	}

	public boolean isEmpty(float x, float y) {
		if (x < 0 || y < 0 || x >= WIDTH || y >= HEIGHT) return false;
		int tile = tiles[(int) (x)+(int)(y)*WIDTH];
		int draw = tile & DRAW_MASK;
		//AtlasSprite s = null;
		//s = tileSprites[draw];
		
		if (tile < EMPTY_LIMIT) return true;
		return false;
	}
	
	public boolean isEmpty(int x, int y) {
		if (x < 0 || y < 0 || x >= WIDTH || y >= HEIGHT) return false;
		int tile = tiles[ (x)+(y)*WIDTH];
		int draw = tile & DRAW_MASK;
		//AtlasSprite s = null;
		//s = tileSprites[draw];
		
		if (tile < EMPTY_LIMIT) return true;
		return false;
	}

	public void setCrossParticle(Vec3i p) {
		particles[p.x  + p.y*WIDTH] = PARTICLE_START+63;
		
	}

	public void setFireParticle(Vec3i p) {
		particles[p.x  + p.y*WIDTH] = PARTICLE_START+63+128;		
	}

	public void setGasParticle(Vec3i p) {
		particles[p.x  + p.y*WIDTH] = PARTICLE_START+63+64;		
	}

	public void resetMap(Player player, EntityGroup playerEntities, EntityGroup enemies, Map map) {
		generateCaves();
		generateBats(enemies);
		generateWizards(enemies);
		findStartPosition();
		player.mapPosition.set(map.startPosition.x, map.startPosition.y, 0);
		player.moveToPosition.set(map.startPosition.x, map.startPosition.y, 0);
		player.position.set(map.startPosition);
		playerEntities.add(player);
		
	}

	public boolean hasHarmfulParticle(int x, int y) {
		return particles[x  + y*WIDTH] > PARTICLE_START+63;		
	}
	public EntityGroup items;
	public void createItem(Vec3i mapPosition) {
		Item item = Pools.obtain(Item.class);
		item.itemID = MathUtils.random(6);
		item.mapPosition.set(mapPosition);
		item.moveToPosition.set(mapPosition);
		item.position.set(mapPosition.x, mapPosition.y);
		item.onSpawn();
		items.add(item);
		
		//Gdx.app.log("hh", "item");
	}

}
